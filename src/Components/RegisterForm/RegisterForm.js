import React, { useState } from 'react';
import moment from 'moment';
import { Radio, Select, DatePicker, Input, Button, Modal, Form, Alert } from 'antd';
import './RegisterForm.css';

const { Option } = Select;
const { TextArea } = Input;
const { confirm } = Modal;

const RegisterForm = props => {
  // hooks
  const [courseType, setCourseType] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [successAlert, setSuccessAlert] = useState(false);

  // Course Data
  const Course1 = [{ key: 'sr', value: 'Short Reports' }, { key: 'ar', value: 'Annual Reports' }, { key: 'pr', value: 'Presentations' }]
  const Course2 = [{ key: 'pr', value: 'Poetry' }, { key: 'ss', value: 'Short Stories' }, { key: 'dr', value: 'Drama' }]
  const Course3 = [{ key: 'wd', value: 'Web Development' }, { key: 'dsd', value: 'Desktop Software Development' }, { key: 'ra', value: 'Research and Analysis' }]

  // hander func
  const onChange = v => {
    setCourseType(v.target.value)

  };

  // handler datePicker
  const onDateChange = (date, dateString) => {
    if (dateString === '20 December, 2019' || dateString === '15 January,2020' || dateString === '1 February, 2020') {
      //Your selected course and subject is not offered beginning from your selected date
      showConfirm()
    }
  };

  // handler modal
  const showConfirm = () => {
    confirm({
      title: 'Sorry',
      content: 'Your selected course and subject is not offered beginning from your selected date',
    });
  }

  // handler submit
  const handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        setIsLoading(true);
        setTimeout(() => {
          setIsLoading(false);
          setSuccessAlert(true);
        }, 3000);
      }
    });
  };

  
  //From
  const { getFieldDecorator } = props.form;

  return (
    <div>
      {successAlert && <Alert message="Data Submitted" type="success" banner closable />}

      <div className="container">
        <div className="formWrapper">
          <div className="courseHeadingContainer">
            <h1 className="courseHeading">Course Registration Form</h1>
          </div>
          <Form onSubmit={handleSubmit}>
            <div className="radioOptionsContainer">
              <div>
                <h2 className="courseHeading">Select Courses</h2>
              </div>
              <div>
                <Radio.Group defaultValue={courseType} buttonStyle="solid" onChange={onChange} value={courseType}>
                  <Radio.Button style={{ marginRight: 10 }} value="1" >Technical Report Writing</Radio.Button>
                  <Radio.Button style={{ marginRight: 10 }} value="2" >English Literature</Radio.Button>
                  <Radio.Button value="3">Computer Sciences</Radio.Button>
                </Radio.Group>
              </div>
            </div>

            <div className="subjectAndDateContainer">
              <div>
                <h2 className="courseHeading">Select Subjects</h2>
                <Form.Item>
                  {getFieldDecorator('selectSubject', {
                    rules: [{ required: true, message: 'Please select your subject!' }],
                  })(
                    <Select
                      showSearch
                      disabled={courseType === 0}
                      style={{ width: 250, marginRight: 10 }}
                      placeholder="Select Subject"
                      optionFilterProp="children"
                    >
                      {
                        courseType === '1' ?
                          Course1.map((item, key) => {
                            return (
                              <Option key={key} value={item.key}>{item.value}</Option>
                            )
                          })
                          :
                          courseType === '2' ?
                            Course2.map((item, key) => {
                              return (
                                <Option key={key} value={item.key}>{item.value}</Option>
                              )
                            })
                            :
                            courseType === '3' ?
                              Course3.map((item, key) => {
                                return (
                                  <Option key={key} value={item.key}>{item.value}</Option>
                                )
                              })
                              :
                              null
                      }
                    </Select>
                  )}
                </Form.Item>
              </div>
              <div>
                <h2 className="courseHeading">Select Start date</h2>
                <Form.Item>
                  {getFieldDecorator('selectDate', {
                    rules: [{ type: 'object', required: true, message: 'Please select your course date!' }],
                  })(

                    <DatePicker
                      disabled={courseType === 0}
                      style={{ width: 250 }}
                      onChange={onDateChange}
                      disabledDate={current => {
                        return current && current < moment().endOf('day');
                      }}
                      format={"D MMMM, YYYY"}
                    />

                  )}
                </Form.Item>
              </div>
            </div>

            <div className="notesContianer">
              <div>
                <h2 className="courseHeading">Additional Notes</h2>
              </div>
              <Form.Item>
                {getFieldDecorator('notes', {
                })(
                  <div>
                    <TextArea
                      rows={6}
                      minLength={20}
                      maxLength={500}
                      placeholder="Additional Notes"
                    />
                  </div>
                )}
              </Form.Item>
            </div>
            <div className="btnContainer">
              <Button type="primary" style={{ width: 200 }} htmlType='submit' loading={isLoading}>
                Submit
          </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  )

}
const WrappedHorizontalLoginForm = Form.create({ name: 'RegisterForm' })(RegisterForm);

export default WrappedHorizontalLoginForm;